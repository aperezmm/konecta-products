# FRONTEND KONECTA TEST

## ¿Cómo correr el Frontend del proyecto?

Descargar o clonar el repositorio Frontend. Los SIGUIENTES pasos sólo serán para Frontend.
Repositorio: https://gitlab.com/aperezmm/konecta-products
### `npm install`

### `npm run dev`

De esta manera estará corriendo el Frontend.
Abrir [http://localhost:4000](http://localhost:4000) para ver la APP desde su navegador.

## ¿Cómo correr el Backend del proyecto?

Descargar o clonar el repositorio Backend. Los SIGUIENTES pasos sólo serán para Backend.

### Tener XAMPP instalado, y correr Apache & MySQL.

Se necesita tener ambos corriendo.
Debe tener el repositorio en la carpeta donde esta instalado XAMPP -> htdocs -> /here git pull .../

## Alejandro Pérez