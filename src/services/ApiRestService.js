import axios from 'axios';

let instance = null;

class ApiRestService {

    constructor(host, port) {
        this.host = host;
        this.port = port;
        this.config = {};
    }

    getCancelToken() {
        return axios.CancelToken.source();
    }

    /**
     * @returns {Promise}
     * @param {*} path 
     * @param {*} params 
     * @param {*} options 
     */
    get(path, params = {}, options = {}, source = 'unknow') {
        // console.log('GET SOURCE----> ', source, { path });
        const url = `${this.host}:${this.port}`;
        return axios.get(`${url}${path}`, {
            ...options,
            params: { ...params }
        });
    }

    /**
     * @returns {Promise}
     * @param {*} path 
     * @param {*} data 
     * @param {*} config 
     */
    post(path, data = {}, config = {}, source = 'unknow') {
        // console.log('POST SOURCE----> ', source, { path });
        const url = `${this.host}:${this.port}`;
        return axios.post(`${url}${path}`, data, config)
    }

    /**
     * @returns {Promise}
     * @param {*} path 
     * @param {*} data 
     * @param {*} config 
     */
    put(path, data = {}, config = {}) {

        const url = `${this.host}:${this.port}`;
        return axios.put(`${url}${path}`, data, config)
    }

    /**
     * @returns {Promise}
     * @param {*} path 
     * @param {*} data 
     * @param {*} config 
     */
     delete(path, data = {}, config = {}, source = 'unknow') {
        // console.log('POST SOURCE----> ', source, { path });
        const url = `${this.host}:${this.port}`;
        return axios.post(`${url}${path}`, data, config)
    }

}

/**@returns {ApiRestService} */
const Service = () => {
    if (!instance) {
        const host = process.env.REACT_APP_API_HOST;
        const port = process.env.REACT_APP_API_HOST_PORT;

        instance = new ApiRestService(host, port);

    }
    return instance;
}

export default Service;
