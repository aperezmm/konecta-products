import React, { useState } from "react";
import { useHistory } from "react-router";

// MATERIAL UI
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import { Alert, AlertTitle } from "@material-ui/lab";
import CloseIcon from "@material-ui/icons/Close";

// SERVICES
import ApiRestService from "../../../services/ApiRestService";

const useStyles = makeStyles((theme, width) => ({
    css_text_field: {
        color: "#717171",
        "&.Mui-focused": {
            color: "#3a5a89",
        },
    },
    css_outlined_input: {
        textTransform: "uppercase",
        "&:hover:not($disabled):not($cssFocused):not($error) $notchedOutline": {
            borderColor: "#3a5a89",
            boxShadow: "inset 0 1px 1px rgb(0 0 0 / 8%), 0 0 8px rgb(102 175 233 / 60%)",
        },
        "&$cssFocused $notchedOutline": {
            borderColor: "#3a5a89", //focused
        },
    },
    notchedOutline: {},
    cssFocused: {},
    register_button: {
        backgroundColor: "#3a5a89 !important",
        "&:hover": {
            boxShadow: "inset 0 1px 1px #3a5a89, 0 0 8px #3a5a89",
        },
    },
    helperText: {
        color: "#fa3e3e",
    },
}));

export function CreateProductComponent(props) {

    const classes = useStyles();
    const history = useHistory();
    const [apiService] = useState(ApiRestService());

    const [form, setForm] = useState({ productName: "", ref: "", price: "", weight: "", category: "", stock: "" });
    const [errorMsgMap, setErrorMsgMap] = useState({});
    const [showRegisterOtherProduct, setShowRegisterOtherProduct] = useState(false);

    function onFormChangeHandler(field, evt) {
        const formCopy = { ...form };
        formCopy[field] = evt.target.value;
        setForm(formCopy);
        setErrorMsgMap({});
    }

    function returnHomeProductsClickHandler() {
        history.push("/");
    }

    function onClickRegisterOtherProduct(){
        setShowRegisterOtherProduct(false);
        setForm({productName: "", ref: "", price: "", weight: "", category: "", stock: "" });
        setErrorMsgMap({});
    }

    function validationFormHandler(form){

        const errorMsgMapCopy = {};

        errorMsgMapCopy.productName = !form.productName
            ? 'El nombre del producto es obligatorio'
            : null;
        
        errorMsgMapCopy.ref = !form.ref
            ? 'La referencia del es obligatorio'
            : null;

        errorMsgMapCopy.price = !form.price
            ? 'El precio del producto es obligatorio'
            : null;

        errorMsgMapCopy.weight = !form.weight
            ? 'El peso del producto es obligatorio'
            : null;

        errorMsgMapCopy.category = !form.category
            ? 'La categoría del producto es obligatorio'
            : null;

        errorMsgMapCopy.stock = !form.stock
            ? 'El stock del producto es obligatorio'
            : null;

        setErrorMsgMap(errorMsgMapCopy);
    }


    function onClickCreateNewProduct() {
        validationFormHandler(form);
        const {productName, ref, price, weight, category, stock } = form;

        if(productName && ref && price && weight && category && stock){
            try {
                apiService.post(`/konecta_backend/api/create_product.php`, {
                    name: productName,
                    ref,
                    price,
                    weight,
                    category,
                    stock,
                })
                    .then((response) => {
                        let { status } = response;

                        if (status === 200) {
                            // console.log(data, status);
                            setShowRegisterOtherProduct(true);
                            setErrorMsgMap({ ...errorMsgMap, success: true });
                        }
                    })
                    .catch((err) => {
                        console.log("Ha ocurrido un error con el servicio de crear producto...");
                        setErrorMsgMap({ ...errorMsgMap, error: "Ha ocurrido un error con el servicio" });
                    });
                setErrorMsgMap({ ...errorMsgMap, success: true });
            } catch (error) {
                setErrorMsgMap({ ...errorMsgMap, error: true });
            }

            
        }
    }

    function closeSuccessRegisterProductClickHandler() {
        setErrorMsgMap({});
        setShowRegisterOtherProduct(false);
        setForm({productName: "", ref: "", price: "", weight: "", category: "", stock: "" });
    }

    return (
        <div>
            <div className="p-3 border-2 m-2 rounded-md">
                <div className="mb-5 text-center">
                    <div className="flex">
                        <ArrowBackIosIcon
                            className="hover:opacity-60 cursor-pointer"
                            onClick={returnHomeProductsClickHandler}
                        ></ArrowBackIosIcon>
                    </div>
                    <div className="text-2xl font-semibold text-gray-800">¡KONECTA!</div>
                    <div className="text-md text-gray-400">Vas a registrar un producto.</div>
                </div>

                {errorMsgMap.success && (
                    <div className="px-5 mb-2 flex justify-center w-full">
                        <Alert severity="success" className="max-w-max w-full">
                            <div className="flex justify-between w-full">
                                <AlertTitle>KONECTA Registro éxitoso</AlertTitle>
                                <CloseIcon
                                    onClick={closeSuccessRegisterProductClickHandler}
                                    className="cursor-pointer"
                                ></CloseIcon>
                            </div>
                            ¡Se ha registrado éxitosamente el producto!
                        </Alert>
                    </div>
                )}

                {errorMsgMap.error && (
                    <div className="px-5 mb-2 flex justify-center w-full">
                        <Alert severity="error" className="max-w-max w-full">
                            <div className="flex justify-between w-full">
                                <AlertTitle>KONECTA Ha ocurrido un error</AlertTitle>
                                <CloseIcon
                                    onClick={closeSuccessRegisterProductClickHandler}
                                    className="cursor-pointer"
                                ></CloseIcon>
                            </div>
                            Ha ocurrido un error con el registro del producto
                        </Alert>
                    </div>
                )}

                <div>
                    <div className="flex flex-col justify-around sm:flex-row">
                        {/* FORM PRODUCT NAME */}
                        <div className="w-full sm:w-1/2 my-2 sm:mr-2">
                            <TextField
                                value={form.productName}
                                onChange={(evt) => onFormChangeHandler("productName", evt)}
                                type="text"
                                className=" w-full"
                                helperText={errorMsgMap.productName}
                                InputLabelProps={{
                                    classes: {
                                        root: classes.css_text_field,
                                        focused: classes.cssFocused,
                                    },
                                }}
                                InputProps={{
                                    classes: {
                                        root: classes.css_outlined_input,
                                        focused: classes.cssFocused,
                                        notchedOutline: classes.notchedOutline,
                                    },
                                }}
                                FormHelperTextProps={{
                                    className: `${classes.helperText}`,
                                }}
                                label="Nombre del producto"
                                variant="outlined"
                            ></TextField>
                        </div>

                        {/* FORM PRODUCT REF */}
                        <div className="w-full sm:w-1/2 my-2 sm:ml-2">
                            <TextField
                                value={form.ref}
                                onChange={(evt) => onFormChangeHandler("ref", evt)}
                                type="text"
                                className=" w-full"
                                helperText={errorMsgMap.ref}
                                InputLabelProps={{
                                    classes: {
                                        root: classes.css_text_field,
                                        focused: classes.cssFocused,
                                    },
                                }}
                                InputProps={{
                                    classes: {
                                        root: classes.css_outlined_input,
                                        focused: classes.cssFocused,
                                        notchedOutline: classes.notchedOutline,
                                    },
                                }}
                                FormHelperTextProps={{
                                    className: `${classes.helperText}`,
                                }}
                                label="Referencia del producto"
                                variant="outlined"
                            ></TextField>
                        </div>
                    </div>

                    <div className="flex flex-col justify-around sm:flex-row">
                        {/* PRODUCT PRICE */}
                        <div className="w-full sm:w-1/2 my-2 sm:mr-2">
                            <TextField
                                value={form.price}
                                onChange={(evt) => onFormChangeHandler("price", evt)}
                                type="number"
                                className=" w-full"
                                helperText={errorMsgMap.price}
                                InputLabelProps={{
                                    classes: {
                                        root: classes.css_text_field,
                                        focused: classes.cssFocused,
                                    },
                                }}
                                InputProps={{
                                    classes: {
                                        root: classes.css_outlined_input,
                                        focused: classes.cssFocused,
                                        notchedOutline: classes.notchedOutline,
                                    },
                                }}
                                FormHelperTextProps={{
                                    className: `${classes.helperText}`,
                                }}
                                label="Precio en ($COP)"
                                variant="outlined"
                            ></TextField>
                        </div>

                        {/* PRODUCT weight */}
                        <div className="w-full sm:w-1/2 my-2 sm:ml-2">
                            <TextField
                                value={form.weight}
                                onChange={(evt) => onFormChangeHandler("weight", evt)}
                                type="number"
                                className=" w-full"
                                helperText={errorMsgMap.weight}
                                InputLabelProps={{
                                    classes: {
                                        root: classes.css_text_field,
                                        focused: classes.cssFocused,
                                    },
                                }}
                                InputProps={{
                                    classes: {
                                        root: classes.css_outlined_input,
                                        focused: classes.cssFocused,
                                        notchedOutline: classes.notchedOutline,
                                    },
                                }}
                                FormHelperTextProps={{
                                    className: `${classes.helperText}`,
                                }}
                                label="Peso en (KG)"
                                variant="outlined"
                            ></TextField>
                        </div>
                    </div>

                    <div className="flex flex-col justify-around sm:flex-row">
                        {/* PRODUCT CATEGORY */}
                        <div className="w-full sm:w-1/2 my-2 sm:mr-2">
                            <TextField
                                value={form.category}
                                onChange={(evt) => onFormChangeHandler("category", evt)}
                                type="text"
                                className=" w-full"
                                helperText={errorMsgMap.category}
                                InputLabelProps={{
                                    classes: {
                                        root: classes.css_text_field,
                                        focused: classes.cssFocused,
                                    },
                                }}
                                InputProps={{
                                    classes: {
                                        root: classes.css_outlined_input,
                                        focused: classes.cssFocused,
                                        notchedOutline: classes.notchedOutline,
                                    },
                                }}
                                FormHelperTextProps={{
                                    className: `${classes.helperText}`,
                                }}
                                label="Categoria"
                                variant="outlined"
                            ></TextField>
                        </div>

                        {/* STOCK CATEGORY */}
                        <div className="w-full sm:w-1/2 my-2 sm:ml-2">
                            <TextField
                                value={form.stock}
                                onChange={(evt) => onFormChangeHandler("stock", evt)}
                                type="number"
                                className=" w-full"
                                helperText={errorMsgMap.stock}
                                InputLabelProps={{
                                    classes: {
                                        root: classes.css_text_field,
                                        focused: classes.cssFocused,
                                    },
                                }}
                                InputProps={{
                                    classes: {
                                        root: classes.css_outlined_input,
                                        focused: classes.cssFocused,
                                        notchedOutline: classes.notchedOutline,
                                    },
                                }}
                                FormHelperTextProps={{
                                    className: `${classes.helperText}`,
                                }}
                                label="Cantidad"
                                variant="outlined"
                            ></TextField>
                        </div>
                    </div>
                </div>
                
                {!showRegisterOtherProduct &&
                <div className={`${classes.register_button} w-1/3 mx-auto flex justify-center p-2 rounded mt-6 cursor-pointer`}>
                    <div className="text-white font-semibold" onClick={onClickCreateNewProduct}>
                        Registrar producto
                    </div>
                </div> }

                {showRegisterOtherProduct &&
                <div className={`${classes.register_button} w-1/3 mx-auto flex justify-center p-2 rounded mt-6 cursor-pointer`}>
                    <div className="text-white font-semibold" onClick={onClickRegisterOtherProduct}>
                        Registrar otro producto
                    </div>
                </div> }
            </div>
        </div>
    );
}
