import React, { useState, useEffect } from "react";

// MATERIAL UI
import Dialog from "@material-ui/core/Dialog";
import { makeStyles } from "@material-ui/core/styles";
import DialogContent from "@material-ui/core/DialogContent";
import TextField from "@material-ui/core/TextField";
import { Alert, AlertTitle } from "@material-ui/lab";
import CloseIcon from "@material-ui/icons/Close";

// SERVICES
import ApiRestService from "../../../services/ApiRestService";

const useStyles = makeStyles((theme, width) => ({
    css_text_field: {
        color: "#717171",
        "&.Mui-focused": {
            color: "#3a5a89",
        },
    },
    css_outlined_input: {
        textTransform: "uppercase",
        "&:hover:not($disabled):not($cssFocused):not($error) $notchedOutline": {
            borderColor: "#3a5a89",
            boxShadow: "inset 0 1px 1px rgb(0 0 0 / 8%), 0 0 8px rgb(102 175 233 / 60%)",
        },
        "&$cssFocused $notchedOutline": {
            borderColor: "#3a5a89", //focused
        },
    },
    notchedOutline: {},
    cssFocused: {},
    update_button: {
        backgroundColor: "#3a5a89 !important",
        "&:hover": {
            boxShadow: "inset 0 1px 1px #3a5a89, 0 0 8px #3a5a89",
        },
    },
    helperText: {
        color: "#fa3e3e",
    },
}));

export function EditProductComponent(props) {
    const { open, productInfo, scrollType, onClose, getProducts} = props;

    // Referencia para saber en qué momento cerrar el dialogo
    const descriptionElementRef = React.useRef(null);

    const classes = useStyles();
    const [apiService] = useState(ApiRestService());

    const [form, setForm] = useState({ productName: "", ref: "", price: "", weight: "", category: "", stock: "" });
    const [errorMsgMap, setErrorMsgMap] = useState({});

    function onFormChangeHandler(field, evt) {
        const formCopy = { ...form };
        formCopy[field] = evt.target.value;
        setForm(formCopy);
    }

    function validationFormHandler(form) {
        const errorMsgMapCopy = {};

        errorMsgMapCopy.productName = !form.productName ? "No puede dejar el producto sin nombre" : null;

        errorMsgMapCopy.ref = !form.ref ? "No puede dejar el producto sin referencia" : null;

        errorMsgMapCopy.price = !form.price ? "No puede dejar el producto sin precio" : null;

        errorMsgMapCopy.weight = !form.weight ? "No puede dejar el producto sin peso" : null;

        errorMsgMapCopy.category = !form.category ? "No puede dejar el producto sin categoria" : null;

        errorMsgMapCopy.stock = !form.stock ? "Debe incluir el STOCK, así sea 0" : null;

        setErrorMsgMap(errorMsgMapCopy);
    }

    function onClickEditInfoProduct() {
        validationFormHandler(form);
        const { productName, ref, price, weight, category, stock } = form;

        if (productName && ref && price && weight && category && stock) {
            try {
                apiService.post(`/konecta_backend/api/edit_product.php`, {
                    id: productInfo.id,
                    name: productName,
                    ref,
                    price,
                    weight,
                    category,
                    stock,
                })
                    .then((response) => {
                        let { status } = response;

                        if (status === 200) {
                            // console.log(data, status);
                            setErrorMsgMap({ ...errorMsgMap, success: true });
                            getProducts();
                        }
                    })
                    .catch((err) => {
                        console.log("Ha ocurrido un error con el servicio...");
                        setErrorMsgMap({ ...errorMsgMap, error: "Ha ocurrido un error con el servicio" });
                    });
                
            } catch (error) {
                setErrorMsgMap({ ...errorMsgMap, error: true });
            }
        }
    }

    function closeEditProductClickHandler() {
        setErrorMsgMap({});
    }

    useEffect(() => {
        if (open) {
            const { current: descriptionElement } = descriptionElementRef;
            if (descriptionElement !== null) {
                descriptionElement.focus();
            }
        }
    }, [open]);

    useEffect(() => {
        // console.log("Product info", productInfo);
        setForm({
            productName: productInfo.name,
            ref: productInfo.ref,
            price: productInfo.price,
            weight: productInfo.weight,
            category: productInfo.category,
            stock: productInfo.stock,
        });
    }, [productInfo]);

    return (
        <div>
            <Dialog
                open={open}
                onClose={onClose}
                maxWidth="xl"
                scroll={scrollType}
                aria-labelledby="scroll-dialog-title"
                aria-describedby="scroll-dialog-description"
            >
                <DialogContent dividers={scrollType === "paper"}>
                    {!productInfo && (
                        <div className="text-center text-2xl text-gray-700 mb-4 font-semibold">
                            Cargando información...
                        </div>
                    )}
                    {productInfo && (
                        <div>
                            <div className="text-center text-2xl text-gray-700 mb-4 font-semibold">
                                Editar información del producto {productInfo.id}
                            </div>

                            {errorMsgMap.success && (
                                <div className="px-5 mb-5 flex justify-center w-full">
                                    <Alert severity="success" className="max-w-max w-full">
                                        <div className="flex justify-between w-full">
                                            <AlertTitle>KONECTA Actualización éxitosa</AlertTitle>
                                            <CloseIcon
                                                onClick={closeEditProductClickHandler}
                                                className="cursor-pointer"
                                            ></CloseIcon>
                                        </div>
                                        ¡Se ha actualizado la información éxitosamente del producto!
                                    </Alert>
                                </div>
                            )}

                            {errorMsgMap.error && (
                                <div className="px-5 mb-5 flex justify-center w-full">
                                    <Alert severity="error" className="max-w-max w-full">
                                        <div className="flex justify-between w-full">
                                            <AlertTitle>KONECTA Ha ocurrido un error</AlertTitle>
                                            <CloseIcon
                                                onClick={closeEditProductClickHandler}
                                                className="cursor-pointer"
                                            ></CloseIcon>
                                        </div>
                                        Ha ocurrido un error al actualizar la información del producto
                                    </Alert>
                                </div>
                            )}

                            <div id="product_info_edit">
                                <div className="flex flex-col justify-around sm:flex-row">
                                    {/* PRODUCT ID */}
                                    <div className="w-full sm:w-1/2 my-2 sm:mr-2">
                                        <TextField
                                            defaultValue={productInfo.id}
                                            disabled
                                            type="number"
                                            className="w-full"
                                            InputLabelProps={{
                                                classes: {
                                                    root: classes.css_text_field,
                                                    focused: classes.cssFocused,
                                                },
                                            }}
                                            InputProps={{
                                                classes: {
                                                    root: classes.css_outlined_input,
                                                    focused: classes.cssFocused,
                                                    notchedOutline: classes.notchedOutline,
                                                },
                                            }}
                                            label="ID del producto"
                                            variant="outlined"
                                        ></TextField>
                                    </div>

                                    {/* PRODUCT NAME */}
                                    <div className="w-full sm:w-1/2 my-2 sm:mr-2">
                                        <TextField
                                            defaultValue={productInfo.name}
                                            onChange={(evt) => onFormChangeHandler("productName", evt)}
                                            type="text"
                                            helperText={errorMsgMap.productName}
                                            className=" w-full"
                                            InputLabelProps={{
                                                classes: {
                                                    root: classes.css_text_field,
                                                    focused: classes.cssFocused,
                                                },
                                            }}
                                            InputProps={{
                                                classes: {
                                                    root: classes.css_outlined_input,
                                                    focused: classes.cssFocused,
                                                    notchedOutline: classes.notchedOutline,
                                                },
                                            }}
                                            FormHelperTextProps={{
                                                className: `${classes.helperText}`,
                                            }}
                                            label="Nombre del producto"
                                            variant="outlined"
                                        ></TextField>
                                    </div>

                                    {/* FORM PRODUCT REF */}
                                    <div className="w-full sm:w-1/2 my-2 sm:ml-2">
                                        <TextField
                                            defaultValue={productInfo.ref}
                                            onChange={(evt) => onFormChangeHandler("ref", evt)}
                                            type="text"
                                            className=" w-full"
                                            helperText={errorMsgMap.ref}
                                            InputLabelProps={{
                                                classes: {
                                                    root: classes.css_text_field,
                                                    focused: classes.cssFocused,
                                                },
                                            }}
                                            InputProps={{
                                                classes: {
                                                    root: classes.css_outlined_input,
                                                    focused: classes.cssFocused,
                                                    notchedOutline: classes.notchedOutline,
                                                },
                                            }}
                                            FormHelperTextProps={{
                                                className: `${classes.helperText}`,
                                            }}
                                            label="Referencia del producto"
                                            variant="outlined"
                                        ></TextField>
                                    </div>
                                </div>

                                <div className="flex flex-col justify-around sm:flex-row">
                                    {/* PRODUCT PRICE */}
                                    <div className="w-full sm:w-1/2 my-2 sm:mr-2">
                                        <TextField
                                            defaultValue={productInfo.price}
                                            onChange={(evt) => onFormChangeHandler("price", evt)}
                                            type="number"
                                            className=" w-full"
                                            helperText={errorMsgMap.price}
                                            InputLabelProps={{
                                                classes: {
                                                    root: classes.css_text_field,
                                                    focused: classes.cssFocused,
                                                },
                                            }}
                                            InputProps={{
                                                classes: {
                                                    root: classes.css_outlined_input,
                                                    focused: classes.cssFocused,
                                                    notchedOutline: classes.notchedOutline,
                                                },
                                            }}
                                            FormHelperTextProps={{
                                                className: `${classes.helperText}`,
                                            }}
                                            label="Precio en ($COP)"
                                            variant="outlined"
                                        ></TextField>
                                    </div>

                                    {/* PRODUCT weight */}
                                    <div className="w-full sm:w-1/2 my-2 sm:ml-2">
                                        <TextField
                                            defaultValue={productInfo.weight}
                                            onChange={(evt) => onFormChangeHandler("weight", evt)}
                                            type="number"
                                            className=" w-full"
                                            helperText={errorMsgMap.weight}
                                            InputLabelProps={{
                                                classes: {
                                                    root: classes.css_text_field,
                                                    focused: classes.cssFocused,
                                                },
                                            }}
                                            InputProps={{
                                                classes: {
                                                    root: classes.css_outlined_input,
                                                    focused: classes.cssFocused,
                                                    notchedOutline: classes.notchedOutline,
                                                },
                                            }}
                                            FormHelperTextProps={{
                                                className: `${classes.helperText}`,
                                            }}
                                            label="Peso en (KG)"
                                            variant="outlined"
                                        ></TextField>
                                    </div>
                                </div>

                                <div className="flex flex-col justify-around sm:flex-row">
                                    {/* PRODUCT CATEGORY */}
                                    <div className="w-full sm:w-1/2 my-2 sm:mr-2">
                                        <TextField
                                            defaultValue={productInfo.category}
                                            onChange={(evt) => onFormChangeHandler("category", evt)}
                                            type="text"
                                            className=" w-full"
                                            helperText={errorMsgMap.category}
                                            InputLabelProps={{
                                                classes: {
                                                    root: classes.css_text_field,
                                                    focused: classes.cssFocused,
                                                },
                                            }}
                                            InputProps={{
                                                classes: {
                                                    root: classes.css_outlined_input,
                                                    focused: classes.cssFocused,
                                                    notchedOutline: classes.notchedOutline,
                                                },
                                            }}
                                            FormHelperTextProps={{
                                                className: `${classes.helperText}`,
                                            }}
                                            label="Categoria"
                                            variant="outlined"
                                        ></TextField>
                                    </div>

                                    {/* STOCK CATEGORY */}
                                    <div className="w-full sm:w-1/2 my-2 sm:ml-2">
                                        <TextField
                                            defaultValue={productInfo.stock}
                                            onChange={(evt) => onFormChangeHandler("stock", evt)}
                                            type="number"
                                            className=" w-full"
                                            helperText={errorMsgMap.stock}
                                            InputLabelProps={{
                                                classes: {
                                                    root: classes.css_text_field,
                                                    focused: classes.cssFocused,
                                                },
                                            }}
                                            InputProps={{
                                                classes: {
                                                    root: classes.css_outlined_input,
                                                    focused: classes.cssFocused,
                                                    notchedOutline: classes.notchedOutline,
                                                },
                                            }}
                                            FormHelperTextProps={{
                                                className: `${classes.helperText}`,
                                            }}
                                            label="Cantidad"
                                            variant="outlined"
                                        ></TextField>
                                    </div>
                                </div>
                            </div>
                            <div
                                className={`${classes.update_button} w-1/3 mx-auto flex justify-center p-2 rounded mt-6 cursor-pointer`}
                            >
                                <div className="text-white font-semibold" onClick={onClickEditInfoProduct}>
                                    Actualizar información
                                </div>
                            </div>
                        </div>
                    )}
                </DialogContent>
            </Dialog>
        </div>
    );
}
