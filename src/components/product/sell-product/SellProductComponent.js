import React, { useEffect, useState } from "react";

// MATERIAL UI
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import { Alert, AlertTitle } from "@material-ui/lab";
import CloseIcon from "@material-ui/icons/Close";

// SERVICES
import ApiRestService from "../../../services/ApiRestService";

const useStyles = makeStyles((theme, width) => ({
    css_text_field: {
        color: "#717171",
        "&.Mui-focused": {
            color: "#3a5a89",
        },
    },
    css_outlined_input: {
        textTransform: "uppercase",
        "&:hover:not($disabled):not($cssFocused):not($error) $notchedOutline": {
            borderColor: "#3a5a89",
            boxShadow: "inset 0 1px 1px rgb(0 0 0 / 8%), 0 0 8px rgb(102 175 233 / 60%)",
        },
        "&$cssFocused $notchedOutline": {
            borderColor: "#3a5a89", //focused
        },
    },
    notchedOutline: {},
    cssFocused: {},
    sell_button: {
        backgroundColor: "#615e9b !important",
        "&:hover": {
            boxShadow: "inset 0 1px 1px #615e9b, 0 0 8px #615e9b",
        },
    },
    helperText: {
        color: "#fa3e3e",
    },
}));

export function SellProductComponent(props) {
    const { open, productInfo, scrollType, onClose, getProducts } = props;

    // Referencia para saber en qué momento cerrar el dialogo
    const descriptionElementRef = React.useRef(null);
    const classes = useStyles();
    const [apiService] = useState(ApiRestService());

    const [form, setForm] = useState({ quantitySold: "" });
    const [errorMsgMap, setErrorMsgMap] = useState({});

    function onFormChangeHandler(field, evt) {
        const formCopy = { ...form };
        formCopy[field] = evt.target.value;
        setForm(formCopy);
    }

    function closeSellProductClickHandler(){
        setErrorMsgMap({});
    }

    function sendProduct(productId, quantitySold){
        try {
            apiService.post(`/konecta_backend/api/sell_product.php`, 
                { id: productId,  stock: quantitySold })
                .then((response) => {
                    let { status } = response;
                    // console.log(data, status);
                    if (status === 200) {
                        setErrorMsgMap({ ...errorMsgMap, success: true });
                        getProducts();
                    }

                    if (status === 400) {
                        setErrorMsgMap({ ...errorMsgMap, noStockAvaible: true });
                    }
                })
                .catch((err) => {
                    console.log("Ha ocurrido un error con el servicio de ventas");
                    setErrorMsgMap({...errorMsgMap, error: "Ha ocurrido un error con el servicio de ventas"});
                });
        } catch (error) {
            setErrorMsgMap({ ...errorMsgMap, error: "Ha ocurrido un error con el servicio de ventas" });
        }
    }

    function clickOnSellProductHandler() {
        const { quantitySold } = form;
        if (!quantitySold) {
            setErrorMsgMap({ ...errorMsgMap, quantitySold: "Debe incluir la cantidad a vender" });
            return;
        }

        if (quantitySold < 0 || quantitySold === 0) {
            setErrorMsgMap({ ...errorMsgMap, quantitySold: "Debe ser mayor a cero y positivo" });
            return;
        }

        if (quantitySold > 0) {
            setErrorMsgMap({});
            const productId = productInfo.id;
            sendProduct(productId, quantitySold);
        }
    }

    useEffect(() => {
        if (open) {
            const { current: descriptionElement } = descriptionElementRef;
            if (descriptionElement !== null) {
                descriptionElement.focus();
            }
        }
    }, [open]);

    // useEffect(() => {
    //     console.log("Product info", productInfo);
    // }, [productInfo]);

    return (
        <div>
            <Dialog
                open={open}
                onClose={onClose}
                maxWidth="xl"
                scroll={scrollType}
                aria-labelledby="scroll-dialog-title"
                aria-describedby="scroll-dialog-description"
            >
                <DialogContent dividers={scrollType === "paper"}>
                    <div>
                        <div className="text-center text-2xl text-gray-700 mb-4 font-semibold">
                            ¡Realiza una buena venta del producto ID {productInfo.id}!
                        </div>

                        {errorMsgMap.success && (
                            <div className="px-5 mb-5 flex justify-center w-full">
                                <Alert severity="success" className="max-w-max w-full">
                                    <div className="flex justify-between w-full">
                                        <AlertTitle>KONECTA Venta éxitosa</AlertTitle>
                                        <CloseIcon
                                            onClick={closeSellProductClickHandler}
                                            className="cursor-pointer"
                                        ></CloseIcon>
                                    </div>
                                    ¡Se ha realizado una venta éxitosamente del producto!
                                </Alert>
                            </div>
                        )}

                        {errorMsgMap.error && (
                            <div className="px-5 mb-5 flex justify-center w-full">
                                <Alert severity="error" className="max-w-max w-full">
                                    <div className="flex justify-between w-full">
                                        <AlertTitle>KONECTA Ha ocurrido un error</AlertTitle>
                                        <CloseIcon
                                            onClick={closeSellProductClickHandler}
                                            className="cursor-pointer"
                                        ></CloseIcon>
                                    </div>
                                    Ha ocurrido un error durante la venta del producto
                                </Alert>
                            </div>
                        )}
                        <div className="">
                            {/* PRODUCT ID */}
                            <div className="w-full mt-10">
                                <TextField
                                    defaultValue={productInfo.id}
                                    disabled
                                    type="number"
                                    className="w-full"
                                    InputLabelProps={{
                                        classes: {
                                            root: classes.css_text_field,
                                            focused: classes.cssFocused,
                                        },
                                    }}
                                    InputProps={{
                                        classes: {
                                            root: classes.css_outlined_input,
                                            focused: classes.cssFocused,
                                            notchedOutline: classes.notchedOutline,
                                        },
                                    }}
                                    label="ID del producto"
                                    variant="outlined"
                                ></TextField>
                            </div>

                            {/* FORM PRODUCT NAME */}
                            <div className="w-full my-5">
                                <TextField
                                    value={form.quantitySold}
                                    onChange={(evt) => onFormChangeHandler("quantitySold", evt)}
                                    type="number"
                                    className=" w-full"
                                    helperText={errorMsgMap.quantitySold}
                                    InputLabelProps={{
                                        classes: {
                                            root: classes.css_text_field,
                                            focused: classes.cssFocused,
                                        },
                                    }}
                                    InputProps={{
                                        classes: {
                                            root: classes.css_outlined_input,
                                            focused: classes.cssFocused,
                                            notchedOutline: classes.notchedOutline,
                                        },
                                    }}
                                    FormHelperTextProps={{
                                        className: `${classes.helperText}`,
                                    }}
                                    label="Cantidad del producto que se venderá"
                                    variant="outlined"
                                ></TextField>
                            </div>
                        </div>

                        <div
                            className={`${classes.sell_button} w-1/3 mx-auto flex justify-center p-2 rounded mt-6 cursor-pointer`}
                        >
                            <div onClick={clickOnSellProductHandler} className={`text-white font-semibold`}>
                                Vender producto
                            </div>
                        </div>
                    </div>
                </DialogContent>
            </Dialog>
        </div>
    );
}
