import React, { useEffect, useState } from "react";

// MATERIAL UI
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import { makeStyles } from "@material-ui/core/styles";
import { Alert, AlertTitle } from "@material-ui/lab";
import CloseIcon from "@material-ui/icons/Close";

// SERVICES
import ApiRestService from "../../../services/ApiRestService";

const useStyles = makeStyles((theme, width) => ({
    css_text_field: {
        color: "#717171",
        "&.Mui-focused": {
            color: "#3a5a89",
        },
    },
    css_outlined_input: {
        textTransform: "uppercase",
        "&:hover:not($disabled):not($cssFocused):not($error) $notchedOutline": {
            borderColor: "#3a5a89",
            boxShadow: "inset 0 1px 1px rgb(0 0 0 / 8%), 0 0 8px rgb(102 175 233 / 60%)",
        },
        "&$cssFocused $notchedOutline": {
            borderColor: "#3a5a89", //focused
        },
    },
    notchedOutline: {},
    cssFocused: {},
    delete_button: {
        backgroundColor: "#3a5a89 !important",
        "&:hover": {
            boxShadow: "inset 0 1px 1px #3a5a89, 0 0 8px #3a5a89",
        },
    },
    cancel_button: {
        backgroundColor: "#dca610 !important",
        "&:hover": {
            boxShadow: "inset 0 1px 1px #dca610, 0 0 8px #dca610",
        },
    },
    helperText: {
        color: "#fa3e3e",
    },
}));

export function DeleteProductComponent(props) {
    const { open, productInfo, scrollType, onClose, getProducts } = props;

    // Referencia para saber en qué momento cerrar el dialogo
    const descriptionElementRef = React.useRef(null);
    const classes = useStyles();
    const [apiService] = useState(ApiRestService());
    const [errorMsgMap, setErrorMsgMap] = useState({});

    function deleteProductClickHandler(productId) {
        apiService
            .delete(`/konecta_backend/api/delete_product.php`, { id: productId })
            .then((response) => {
                let { status } = response;

                if (status === 200) {
                    setErrorMsgMap({ ...errorMsgMap, success: true });
                    getProducts();
                }
            })
            .catch((err) => {
                console.log("Ha ocurrido un error con el servicio de eliminar...");
                setErrorMsgMap({ ...errorMsgMap, error: "Ha ocurrido un error con el servicio" });
            });
    }

    function closeDeleteProductClickHandler() {
        setErrorMsgMap({});
    }

    useEffect(() => {
        if (open) {
            const { current: descriptionElement } = descriptionElementRef;
            if (descriptionElement !== null) {
                descriptionElement.focus();
            }
        }
    }, [open]);

    // useEffect(() => {
    //     console.log("Product info", productInfo);
    // },[productInfo]);

    return (
        <div>
            <Dialog
                open={open}
                onClose={onClose}
                maxWidth="xl"
                scroll={scrollType}
                aria-labelledby="scroll-dialog-title"
                aria-describedby="scroll-dialog-description"
            >
                <DialogContent dividers={scrollType === "paper"}>
                    {productInfo && (
                        <div>
                            <div className="text-center text-2xl text-gray-700 mb-2 font-semibold">
                                ¿Está seguro que desea eliminar el producto {productInfo.id}?
                            </div>
                            <div className="text-center text-md text-gray-500 mb-4 font-semibold">
                                {productInfo.name}
                            </div>
                            <div className="text-center text-md text-gray-500 mb-4 font-semibold">
                                Referencia = {productInfo.ref}
                            </div>

                            {errorMsgMap.success && (
                                <div className="px-5 mb-5 flex justify-center w-full">
                                    <Alert severity="success" className="max-w-max w-full">
                                        <div className="flex justify-between w-full">
                                            <AlertTitle>KONECTA Actualización éxitosa</AlertTitle>
                                            <CloseIcon
                                                onClick={closeDeleteProductClickHandler}
                                                className="cursor-pointer"
                                            ></CloseIcon>
                                        </div>
                                        ¡Se ha actualizado la información éxitosamente del producto!
                                    </Alert>
                                </div>
                            )}

                            {errorMsgMap.error && (
                                <div className="px-5 mb-5 flex justify-center w-full">
                                    <Alert severity="error" className="max-w-max w-full">
                                        <div className="flex justify-between w-full">
                                            <AlertTitle>KONECTA Ha ocurrido un error</AlertTitle>
                                            <CloseIcon
                                                onClick={closeDeleteProductClickHandler}
                                                className="cursor-pointer"
                                            ></CloseIcon>
                                        </div>
                                        Ha ocurrido un error al actualizar la información del producto
                                    </Alert>
                                </div>
                            )}

                            {!errorMsgMap.success && (
                                <div className="flex flex-col sm:flex-row">
                                    <div
                                        className={`${classes.cancel_button} w-1/3 mx-auto flex justify-center p-2 rounded mt-6 cursor-pointer text-white font-semibold`}
                                        onClick={onClose}
                                    >
                                        No, regresar
                                    </div>
                                    <div
                                        className={`${classes.delete_button} w-1/3 mx-auto flex justify-center p-2 rounded mt-6 cursor-pointer text-white font-semibold`}
                                        onClick={deleteProductClickHandler.bind(this, productInfo.id)}
                                    >
                                        Sí, quiero eliminarlo
                                    </div>
                                </div>
                            )}
                        </div>
                    )}
                </DialogContent>
            </Dialog>
        </div>
    );
}
