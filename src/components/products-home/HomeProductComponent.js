import React, { useState, useEffect } from "react";
import { useHistory } from "react-router";

// MATERIAL UI
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import EditIcon from "@material-ui/icons/Edit";
import MonetizationOnIcon from "@material-ui/icons/MonetizationOn";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import AddCircleIcon from '@material-ui/icons/AddCircle';

// COMPONENTS
import { DeleteProductComponent } from "./../product/delete-product/DeleteProductComponent";
import { EditProductComponent } from "./../product/edit-product/EditProductComponent";
import { SellProductComponent } from "./../product/sell-product/SellProductComponent";

// SERVICES
import ApiRestService from "../../services/ApiRestService";

const StyledTableCell = withStyles((theme) => ({
    head: {
        textTransform: "uppercase",
        fontWeight: "bold",
        color: "#615e9b",
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        "&:nth-of-type(odd)": {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

export function HomeProductComponent() {
    const history = useHistory();
    const [apiService] = useState(ApiRestService());

    const [deleteProductDialogOpen, setDeleteProductDialogOpen] = useState(false);
    const [editProductDialogOpen, setEditProductDialogOpen] = useState(false);
    const [sellProductDialogOpen, setSellProductDialogOpen] = useState(false);
    const [productsList, setProductsList] = useState(null);
    const [alertMsg, setAlertMsg] = useState({});

    const [productInfo, setProductInfo] = useState(null);

    function editProductClickHandler(product) {
        setEditProductDialogOpen(true);
        setProductInfo(product);
    }

    function onCloseEditProductDialog() {
        setEditProductDialogOpen(false);
    }

    function deleteProductClickHandler(product) {
        setDeleteProductDialogOpen(true);
        setProductInfo(product);
    }

    /** Cerramos el dialogo de eliminar producto. */
    function onCloseDeleteProductDialog() {
        setDeleteProductDialogOpen(false);
        //Cuando borremos debemos hacer llamado para actualizar la información.
    }

    /**  */
    function sellProductClickHandler(product) {
        setProductInfo(product);
        setSellProductDialogOpen(true);
    }

    function onCloseSellProductDialog() {
        setSellProductDialogOpen(false);
    }

    function createNewProductClickHandler() {
        history.push("/create/new/product");
    }

    function getProductsList(){
        apiService
            .get(`/konecta_backend/api/list_products.php`)
            .then((response) => {
                let { data, status } = response;

                if (status === 200) {
                    // console.log(data.products);
                    setProductsList(data.products);
                }
            })
            .catch((err) => {
                console.log("Ha ocurrido un error con el servicio de lista...");
                setAlertMsg({ ...alertMsg, error: "Ha ocurrido un error con el servicio" });
            });
    }

    useEffect(() => {
        // console.log("Trayendo los productos...");
        getProductsList();
        // eslint-disable-next-line
    }, []);

    return (
        <div>
            <div className="p-3 border-2 m-2 rounded-md">
                <div className="mb-2">
                    <div className="text-2xl font-semibold text-gray-800">¡Bienvenido, KONECTA!</div>
                    <div className="text-xs text-gray-400 pl-2">Seleccione lo que desea realizar.</div>
                </div>
                <div className="text-center my-2">
                    <div className="text-xl font-semibold text-gray-700">Listado de productos</div>
                </div>

                <div className="flex justify-end">
                    <div
                        className="py-2 border-2 rounded-lg w-1/6 flex justify-around mr-2 bg-gray-600 border-gray-700 text-white px-2 cursor-pointer text-center"
                        onClick={createNewProductClickHandler}
                    >
                        <AddCircleIcon></AddCircleIcon>
                        Nuevo producto
                    </div>
                </div>

                {/* FIXME: DEBERÍA SER UN COMPONENTE APARTE. */}
                <div className="mt-2">
                    {productsList && (
                        <div className="sm:m-2 border-2 rounded my-2" id="table_products">
                            <TableContainer component={Paper}>
                                <Table className={``} aria-label="customized table">
                                    <TableHead>
                                        <TableRow className="border-b-2 bg-gray-300">
                                            <StyledTableCell>ID</StyledTableCell>
                                            <StyledTableCell align="left">Nombre</StyledTableCell>
                                            <StyledTableCell align="center">Referencia&nbsp;</StyledTableCell>
                                            <StyledTableCell align="center">Precio(COP)&nbsp;</StyledTableCell>
                                            <StyledTableCell align="center">Peso(KG)</StyledTableCell>
                                            <StyledTableCell align="center">Categoría</StyledTableCell>
                                            <StyledTableCell align="center">STOCK</StyledTableCell>
                                            <StyledTableCell align="center">Fecha</StyledTableCell>
                                            <StyledTableCell align="center">Acciones</StyledTableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {productsList.map((product, index) => (
                                            <StyledTableRow key={index}>
                                                <StyledTableCell component="th" scope="row" className="border-r-2">
                                                    {product.id}
                                                </StyledTableCell>
                                                <StyledTableCell align="left" className="border-r-2">{product.name}</StyledTableCell>
                                                <StyledTableCell align="center" className="border-r-2">{product.ref}</StyledTableCell>
                                                <StyledTableCell align="center" className="border-r-2">${product.price}</StyledTableCell>
                                                <StyledTableCell align="center" className="border-r-2">{product.weight}</StyledTableCell>
                                                <StyledTableCell align="center" className="border-r-2">{product.category}</StyledTableCell>
                                                <StyledTableCell align="center" className="border-r-2">{product.stock}</StyledTableCell>
                                                <StyledTableCell align="center" className="border-r-2">{product.created_at}</StyledTableCell>
                                                <StyledTableCell align="center">
                                                    <div
                                                        className="flex mx-auto font-semibold border-2 rounded-3xl w-11 justify-around items-center border-2 border-blue-300 bg-blue-200  text-gray-700 cursor-pointer"
                                                        onClick={editProductClickHandler.bind(this, product)}
                                                    >
                                                        <EditIcon></EditIcon>
                                                    </div>
                                                    <div
                                                        className="flex mx-auto font-semibold border-2 rounded-3xl w-11 justify-around items-center bg-red-200 border-red-300 text-gray-700 cursor-pointer my-2"
                                                        onClick={deleteProductClickHandler.bind(this, product)}
                                                    >
                                                        <DeleteForeverIcon></DeleteForeverIcon>
                                                    </div>
                                                    <div
                                                        className="flex mx-auto font-semibold border-2 rounded-3xl w-11 justify-around items-center bg-green-200 border-green-300 text-gray-700 cursor-pointer"
                                                        onClick={sellProductClickHandler.bind(this, product)}
                                                    >
                                                        <MonetizationOnIcon></MonetizationOnIcon>
                                                    </div>
                                                </StyledTableCell>
                                            </StyledTableRow>
                                        ))}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </div>
                    )}

                    {alertMsg.error && (
                        <div className="text-center">
                            <div className="text-2xl text-gray-700 font-semibold">
                                Ha ocurrido un error, lo sentimos.
                            </div>
                            <div className="text-xl text-gray-800">Vuelve a intentarlo en unos minutos.</div>
                        </div>
                    )}
                </div>

                {/* DELETE PRODUCT DIALOG */}
                {deleteProductDialogOpen && (
                    <DeleteProductComponent
                        open={deleteProductDialogOpen}
                        scrollType="body"
                        onClose={onCloseDeleteProductDialog}
                        productInfo={productInfo}
                        getProducts={getProductsList}
                    ></DeleteProductComponent>
                )}

                {/* EDIT PRODUCT DIALOG */}
                {editProductDialogOpen && (
                    <EditProductComponent
                        open={editProductDialogOpen}
                        scrollType="body"
                        onClose={onCloseEditProductDialog}
                        productInfo={productInfo}
                        getProducts={getProductsList}
                    ></EditProductComponent>
                )}

                {sellProductDialogOpen && (
                    <SellProductComponent
                        open={sellProductDialogOpen}
                        scrollType="body"
                        onClose={onCloseSellProductDialog}
                        productInfo={productInfo}
                        getProducts={getProductsList}
                    ></SellProductComponent>
                )}
            </div>
        </div>
    );
}
