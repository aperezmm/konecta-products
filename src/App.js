import './App.css';

import { BrowserRouter,  Switch, Route } from 'react-router-dom';

// COMPONENTS
import { HomeProductComponent } from './components/products-home/HomeProductComponent';
import {CreateProductComponent} from './components/product/create-product/CreateProductComponent';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          <HomeProductComponent />
        </Route>

        <Route exact path="/create/new/product">
          <CreateProductComponent />
        </Route>

      </Switch>
    </BrowserRouter>
  );
}

export default App;
